require("scripts/globals/status")
require("scripts/globals/magic")
require("scripts/globals/msg")

xi = xi or {}
xi.chargedammo = xi.chargedammo or {}

xi.chargedAmmoType =
{
    WIND = 1,
    LIGHTNING = 2,
    WATER = 3
    -- virtue stones are in attackround.cpp function "CreateAttacks"
}

xi.chargedAmmoSubEffect =
{
    [xi.chargedAmmoType.WIND]      = xi.subEffect.WIND_DAMAGE, 
    [xi.chargedAmmoType.LIGHTNING] = xi.subEffect.LIGHTNING_DAMAGE, 
    [xi.chargedAmmoType.WATER]     = xi.subEffect.WATER_DAMAGE
}

xi.chargedAmmoItemID =
{
    [xi.chargedAmmoType.WIND]      = { 18236, 18237, 18238 }, -- wind fan, kilo fan, mega fan
    [xi.chargedAmmoType.LIGHTNING] = { 18228, 18229, 18230 }, -- battery, kilo battery, mega battery
    [xi.chargedAmmoType.WATER]     = { 18232, 18233, 18234 }  -- hydro pump, kilo pump, mega pump
}

xi.chargedAmmoElement =
{
    [xi.chargedAmmoType.WIND]      = xi.magic.ele.WIND,
    [xi.chargedAmmoType.LIGHTNING] = xi.magic.ele.LIGHTNING,
    [xi.chargedAmmoType.WATER]     = xi.magic.ele.WATER
}

function doChargedAmmoAddEffect(player, target, damageType)
    if math.random(1,7) ~= 1 then return 0,0,0 end
    local ammo = player:getEquippedItem(xi.slot.AMMO)
    if ammo == nil then return 0,0,0 end
    
    -- convert ammo type to tier (1,2,3)
    local ammoID = ammo:getID()
    local tier = 0
    if     xi.chargedAmmoItemID[damageType][1] == ammoID then tier = 1
    elseif xi.chargedAmmoItemID[damageType][2] == ammoID then tier = 2
    elseif xi.chargedAmmoItemID[damageType][3] == ammoID then tier = 3
    else return 0,0,0 end
    
    -- player is confirmed to have correct ammo type on. consume 1 ammo
    if ammo:getQuantity() == 1 then player:unequipItem(xi.slot.AMMO) end
    if player:delItem(ammoID, 1, ammo:getLocationID(), ammo:getSlotID()) == false then return 0,0,0 end
    
    -- dmg pre-calc based on tier of ammo
    local dmg = 0
    if     tier == 1 then dmg = math.random(25,50)
    elseif tier == 2 then dmg = math.random(35,70)
    elseif tier == 3 then dmg = math.random(50,100) end
    local levelcorr = (player:getMainLvl() - target:getMainLvl()) / 2
    if levelcorr > 6 then levelcorr = 6 end
    dmg = dmg + levelcorr
    if dmg < 0 then dmg = 0 end
    
    -- dmg calc
    local params = { }
    params.bonusmab = 0
    params.includemab = false
    dmg = addBonusesAbility(player, xi.chargedAmmoElement[damageType], target, dmg, params)
    dmg = dmg * applyResistanceAddEffect(player, target, xi.chargedAmmoElement[damageType], 0)
    dmg = adjustForTarget(target, dmg, xi.chargedAmmoElement[damageType])
    dmg = finalMagicNonSpellAdjustments(player, target, xi.chargedAmmoElement[damageType], dmg)
    
    local msg = xi.msg.basic.ADD_EFFECT_DMG
    if dmg < 0 then
        msg = xi.msg.basic.ADD_EFFECT_HEAL
        dmg = -dmg
    end
    
    return xi.chargedAmmoSubEffect[damageType],msg,dmg
end