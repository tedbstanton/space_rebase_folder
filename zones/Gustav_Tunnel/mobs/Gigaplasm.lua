-----------------------------------
-- Area: Gustav Tunnel
--   NM: Gigaplasm
-- Note: Part of mission "The Salt of the Earth"
-----------------------------------
require("scripts/globals/status")
-----------------------------------
local entity = {}

entity.onMobInitialize = function(mob)
    mob:setMobMod(xi.mobMod.IDLE_DESPAWN, 180)
    mob:setMod(xi.mod.DELAY, 500)
end

entity.onMobDeath = function(mob, player, isKiller)
    if (isKiller) then
        local mobId = mob:getID()
        local x = mob:getXPos()
        local y = mob:getYPos()
        local z = mob:getZPos()

        SpawnMob(mobId + 1):setPos(x, y, z)
        SpawnMob(mobId + 2):setPos(x-1, y, z-1)
    end
end

return entity
