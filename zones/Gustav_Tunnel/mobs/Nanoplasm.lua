-----------------------------------
-- Area: Gustav Tunnel
--   NM: Nanoplasm
-- Note: Part of mission "The Salt of the Earth"
-----------------------------------
require("scripts/globals/status")
-----------------------------------
local entity = {}

entity.onMobInitialize = function(mob)
    mob:setMobMod(xi.mobMod.IDLE_DESPAWN, 180)
    mob:setMod(tpz.mod.DELAY, 150)
    mob:setMod(tpz.mod.DMGMAGIC, 400)

end

entity.onMobDeath = function(mob, player, isKiller)
end

return entity
