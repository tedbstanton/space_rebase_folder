-----------------------------------------
-- ID: 16672
-- Item: Tigerhunter
-- Additional Effect: Paralyzses Tigers

----------------------------------
require("scripts/globals/status")
require("scripts/globals/magic")
require("scripts/globals/msg")
-----------------------------------
local item_object = {}

item_object.onAdditionalEffect = function(player,target,damage)
    if (target:getFamily() == 242) then
        local chance = 15
        if not target:hasStatusEffect(xi.effect.PARALYSIS) and math.random(100) < chance and applyResistanceAddEffect(player,target,xi.magic.ele.ICE,0) > 0.5 then
            target:addStatusEffect(xi.effect.PARALYSIS, 20, 0, 30)
            return xi.subEffect.PARALYSIS, xi.msg.basic.ADD_EFFECT_STATUS, xi.effect.PARALYSIS
        end
    end
    return 0, 0, 0
end

return item_object