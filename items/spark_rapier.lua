-----------------------------------------
-- ID: 
-- Item: 
-- Additional Effect: 
-----------------------------------------
require("scripts/globals/status")
require("scripts/globals/magic")
require("scripts/globals/msg")
require("scripts/globals/chargedammo")
-----------------------------------
local item_object = {}

item_object.onAdditionalEffect = function(player, target, damage)
    return doChargedAmmoAddEffect(player, target, xi.chargedAmmoType.LIGHTNING)
end

return item_object