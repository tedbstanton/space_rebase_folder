-----------------------------------
-- Ability: Wizard's Roll
-- Enhances magic attack for party members within area of effect
-- Optimal Job: Black Mage
-- Lucky Number: 5
-- Unlucky Number: 9
-- Level 58
-- Phantom Roll +1 Value: 2
-- {2, 3, 4, 4, 10, 5, 6, 7, 1, 7, 12, 4}
-- Die Roll    |No BLM  |With BLM
-- --------    -------- -----------
-- 1           |+2      |+6
-- 2           |+3      |+7
-- 3           |+4      |+8
-- 4           |+4      |+8
-- 5           |+10     |+14
-- 6           |+5      |+9
-- 7           |+6      |+10
-- 8           |+7      |+11
-- 9           |+1      |+5
-- 10          |+7      |+11
-- 11          |+12     |+16
-- Bust        |-4      |-4

--
-- If the Corsair is a lower level than the player receiving Wizard's Roll, the +MAB will be reduced
-----------------------------------
require("scripts/globals/job_utils/corsair")
-----------------------------------
local ability_object = {}

ability_object.onAbilityCheck = function(player, target, ability)
    return xi.job_utils.corsair.onRollAbilityCheck(player, target, ability)
end

ability_object.onUseAbility = function(caster, target, ability, action)
    return xi.job_utils.corsair.onRollUseAbility(caster, target, ability, action)
end

return ability_object
