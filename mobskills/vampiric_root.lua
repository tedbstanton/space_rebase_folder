-----------------------------------
-- Vampiric Root
-- Steals HP from a single target and absorbs positive status effects (except food and reraise). 
-- Type: Magical
-- Utsusemi/Blink absorb: 1 shadow
-- Range: Melee
-- Notes: If used against undead, it will simply do damage and not drain HP.
-----------------------------------
require("scripts/globals/mobskills")
require("scripts/globals/msg")
require("scripts/globals/settings")
require("scripts/globals/status")
-----------------------------------
local mobskill_object = {}

mobskill_object.onMobSkillCheck = function(target, mob, skill)
    return 0
end

mobskill_object.onMobWeaponSkill = function(target, mob, skill)
    local dmgmod = 1
    local info = xi.mobskills.mobMagicalMove(mob, target, skill, mob:getWeaponDmg()*3, xi.magic.ele.DARK, dmgmod, xi.mobskills.magicalTpBonus.NO_EFFECT)
    local dmg = xi.mobskills.mobFinalAdjustments(info.dmg, mob, skill, target, xi.attackType.MAGICAL, xi.damageType.DARK, xi.mobskills.shadowBehavior.NUMSHADOWS_1)

        -- will steal beneficials if it actually hits
        if skill:getMsg() ~= xi.msg.basic.SHADOW_ABSORB then
            -- steals all beneficial buffs except food and reraise
            local beneficials =
            {
                -- bar spells
                xi.effect.BARFIRE, xi.effect.BARBLIZZARD, xi.effect.BARAERO, xi.effect.BARSTONE, xi.effect.BARTHUNDER, xi.effect.BARWATER, xi.effect.BARSLEEP,
                xi.effect.BARPOISON, xi.effect.BARPARALYZE, xi.effect.BARBLIND, xi.effect.BARSILENCE, xi.effect.BARPETRIFY, xi.effect.BARVIRUS, xi.effect.BARAMNESIA,
                -- boost and gain spells
                xi.effect.STR_BOOST, xi.effect.DEX_BOOST, xi.effect.VIT_BOOST, xi.effect.AGI_BOOST, xi.effect.INT_BOOST, xi.effect.MND_BOOST, xi.effect.CHR_BOOST,
                -- enspells
                xi.effect.ENFIRE, xi.effect.ENBLIZZARD, xi.effect.ENAERO, xi.effect.ENSTONE, xi.effect.ENTHUNDER, xi.effect.ENWATER,
                xi.effect.ENFIRE_II, xi.effect.ENBLIZZARD_II, xi.effect.ENAERO_II, xi.effect.ENSTONE_II, xi.effect.ENTHUNDER_II, xi.effect.ENWATER_II,
                xi.effect.ENLIGHT, xi.effect.ENDARK,
                -- protect/shell/haste/regen type spells
                xi.effect.BLINK, xi.effect.STONESKIN, xi.effect.AQUAVEIL, xi.effect.PROTECT, xi.effect.SHELL, xi.effect.PHALANX,
                xi.effect.HASTE, xi.effect.FLURRY, xi.effect.FLURRY_II, xi.effect.REGEN, xi.effect.REFRESH, xi.effect.EMBRAVA,
                xi.effect.REGAIN, xi.effect.POTENCY, xi.effect.PAX, xi.effect.INTENSION, xi.effect.REPRISAL,
                -- spikes spells
                xi.effect.BLAZE_SPIKES, xi.effect.ICE_SPIKES, xi.effect.SHOCK_SPIKES, -- xi.effect.DREAD_SPIKES,
                -- storm spells
                xi.effect.FIRESTORM, xi.effect.HAILSTORM, xi.effect.WINDSTORM, xi.effect.SANDSTORM, xi.effect.THUNDERSTORM, xi.effect.RAINSTORM,
                xi.effect.AURORASTORM, xi.effect.VOIDSTORM,
                -- stat boost from misc sources
                xi.effect.MAX_HP_BOOST, xi.effect.MAX_MP_BOOST, xi.effect.ACCURACY_BOOST, xi.effect.ATTACK_BOOST, xi.effect.EVASION_BOOST, xi.effect.DEFENSE_BOOST,
                xi.effect.SHINING_RUBY, xi.effect.MAGIC_ATK_BOOST, xi.effect.MAGIC_DEF_BOOST,
                -- bard songs
                xi.effect.PAEON, xi.effect.BALLAD, xi.effect.MINNE, xi.effect.MINUET, xi.effect.MADRIGAL, xi.effect.PRELUDE,
                xi.effect.MAMBO, xi.effect.AUBADE, xi.effect.PASTORAL, xi.effect.HUM, xi.effect.FANTASIA, xi.effect.OPERETTA,
                xi.effect.CAPRICCIO, xi.effect.SERENADE, xi.effect.ROUND, xi.effect.GAVOTTE, xi.effect.FUGUE, xi.effect.RHAPSODY,
                xi.effect.ARIA, xi.effect.MARCH, xi.effect.ETUDE, xi.effect.CAROL, xi.effect.SIRVENTE, xi.effect.DIRGE,
                xi.effect.SCHERZO,
                -- corsair rolls
                xi.effect.FIGHTERS_ROLL, xi.effect.MONKS_ROLL, xi.effect.HEALERS_ROLL, xi.effect.WIZARDS_ROLL, xi.effect.WARLOCKS_ROLL, xi.effect.ROGUES_ROLL,
                xi.effect.GALLANTS_ROLL, xi.effect.CHAOS_ROLL, xi.effect.BEAST_ROLL, xi.effect.CHORAL_ROLL, xi.effect.HUNTERS_ROLL, xi.effect.SAMURAI_ROLL,
                xi.effect.NINJA_ROLL, xi.effect.DRACHEN_ROLL, xi.effect.EVOKERS_ROLL, xi.effect.MAGUSS_ROLL, xi.effect.CORSAIRS_ROLL, xi.effect.PUPPET_ROLL,
                xi.effect.DANCERS_ROLL, xi.effect.SCHOLARS_ROLL, xi.effect.BOLTERS_ROLL, xi.effect.CASTERS_ROLL, xi.effect.COURSERS_ROLL, xi.effect.BLITZERS_ROLL,
                xi.effect.TACTICIANS_ROLL, xi.effect.ALLIES_ROLL, xi.effect.MISERS_ROLL, xi.effect.COMPANIONS_ROLL, xi.effect.AVENGERS_ROLL, xi.effect.NATURALISTS_ROLL,
                -- dancer sambas
                xi.effect.DRAIN_SAMBA, xi.effect.ASPIR_SAMBA, xi.effect.HASTE_SAMBA,
            }
    
            for i, effect in ipairs(beneficials) do
                if target:hasStatusEffect(effect) then
                    local currentEffect = target:getStatusEffect(effect)
                    MobStatusEffectMove(target, mob, effect, currentEffect:getPower(), currentEffect:getTick(), currentEffect:getTimeRemaining() / 1000)
                    target:delStatusEffect(effect)
                end
            end
        end
    
        skill:setMsg(MobPhysicalDrainMove(mob, target, skill, MOBDRAIN_HP, dmg))
    
    return dmg
end

return mobskill_object
