---------------------------------------------------
-- Vulcanian Impact
-- Deals Fire damage to a single target
---------------------------------------------------
require("scripts/globals/settings")
require("scripts/globals/status")
require("scripts/globals/mobskills")
---------------------------------------------------

mobskill_object.onMobSkillCheck = function(target, mob, skill)
    -- if mob is a Pandemonium Lamp
    if mob:getID() >= 17056169 and mob:getID() <= 17056185 then
        -- only bomb form is allowed to use
        local mobSkin = mob:getModelId()
        if (mobSkin ~= 281) then
            return 1
        end
    end

    return 0
end

mobskill_object.onMobWeaponSkill = function(target, mob, skill)
    local dmgmod = 1
    local info = MobMagicalMove(mob, target, skill, mob:getWeaponDmg()*5, xi.magic.ele.FIRE, dmgmod, TP_NO_EFFECT)
    local dmg = MobFinalAdjustments(info.dmg, mob, skill, target, xi.attackType.MAGICAL, xi.damageType.FIRE, MOBPARAM_IGNORE_SHADOWS)

    target:takeDamage(dmg, mob, xi.attackType.MAGICAL, xi.damageType.FIRE)
    return dmg
end

return mobskill_object