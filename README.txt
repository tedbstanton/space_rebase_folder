== for all mobs for ZNM, should double check skill/spell IDs in the onMobFight ==
== I did not touch IDs.lua, qm(#).lua, or any sqls.


frog_chorus.lua -- confirm costume id on line 27
hellstorm.lua -- check pandimonium lamp id, lamp skin id too?
vulcanian_impact.lua -- check lamp id/skin like w/ hellstorm

Dextrose.lua -- How do we deal with Ebony puddings IDs now?
iriri_samariri.lua == same as above

status.lua i changed the # for familylink and sight_angle, implications?

mobskills/meteor.lua needs to be deleted (we have a spell meteor)

They dont have monstertpmoves.lua, code lives in mobskills.lua

wyrm_wakeup.lua = not in a MR, i had to make it... is there other stuff missing??
Ouryu.lua, had to add the wakeup.lua and baiwingmultiplier logic
charm.lua= instead of reseting emnity on all the moves that charm, i edited the effect to shed emnity, need to test
khimaira.lua = they have stuff that looks fairly different, may want to compare all kings?

pets/flame_breath : local bonus = master:getMerit(tpz.merit.STRAFE)  -- i noticed our code has this, but bonus is nver called.
same w/ frost breath/gust breath/hydro/etc for drg breaths

local entity = {}

entity.onMobInitialize = function(mob)
entity.onMobSpawn = function(mob)
entity.onMobFight = function(mob, target)
entity.onMobWeaponSkill = function(target, mob, skill)
entity.onMobWeaponSkillPrepare = function(mob, target)
entity.onMobDisengage = function(mob)
entity.onMobDespawn = function(mob)
entity.onMobDeath = function(mob, player, isKiller)